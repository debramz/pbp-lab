import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import './widgets/main_drawer.dart';
import './widgets/main_container.dart';
import './widgets/bottom_appbar.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {

  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'D06 | Forum',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(title: 'Forum'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) => Scaffold(
    backgroundColor: Color(0xFFCAB7A1),
      drawer: MainDrawer(),
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        backgroundColor: Color(0xFFB29576),
        title: Text(widget.title, style: GoogleFonts.mrDafoe(textStyle:TextStyle(color: Color(0xFF811112), fontSize: 45)))
      ),
      body: SingleChildScrollView(
      child:MainContainerState(),
      ),
    bottomNavigationBar: BottomNav(),
    floatingActionButton: FloatingActionButton(
      backgroundColor: Color(0xFF811112),
        child: Icon(Icons.add, color:Color(0xFFCAB7A1)), onPressed: () {}),
  floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
  );
  }
