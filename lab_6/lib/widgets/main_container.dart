import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MainContainerState extends StatefulWidget {
  @override
  MainContainer createState() => MainContainer();
}

class MainContainer extends State<MainContainerState>{
  final double contWidth = 350.0;
  final double contHeight = 250.0;
  int _likeCount = 0;
  int _dislikeCount = 0;
  bool disliked = false;
  bool liked = false;


  void _incrementLike() {
    setState (() {
      if (liked != true){
        _likeCount++;
        liked = true;
      }
      if (disliked == true){
        disliked = false;
        _dislikeCount--;
      }
    });
  }

  void _decrementLike() {
    setState(() {
      _likeCount--;
      liked = false;
    });
  }

  void _incrementDislike() {
    setState (() {
      if (disliked != true) {
        _dislikeCount++;
        disliked = true;
      }
      if (liked == true){
        liked = false;
        _likeCount--;
      }
    });
  }

  void _decrementDislike() {
    setState(() {
      _dislikeCount--;
      disliked = false;
    });
  }

  @override
  Widget build(BuildContext context){
    return Center(
        child: Column(children: <Widget>[
      Container(
          color: Color(0xffAC3834),
          width:contWidth,
          height:contHeight,
          alignment: Alignment.center,
          margin: EdgeInsets.all(25.0),
          padding: EdgeInsets.all(20.0),
          child: Column(
              children:<Widget>[
            Row(
                children:<Widget>[
              Container(
                alignment:Alignment.centerLeft,
                child: Image.network('https://live.staticflickr.com/65535/51634618240_caba66ca65_o.png', width: 60, height:60)
              ),
              Column(children:<Widget>[
                Container(
                  width:200,
                  alignment: Alignment.centerLeft,
                  child: Text("POST TITLE 1",
                  style: GoogleFonts.bebasNeue(textStyle:TextStyle(color: Color(0xFF811112), fontSize:30)),
                  )
                ),
                Container(
                  width:200,
                  alignment: Alignment.centerLeft,
                    child: Text("Posted by Anonymous",
                        style: GoogleFonts.sora(textStyle:TextStyle(
                            color: Color(0xFFB29576), letterSpacing:1.3, fontSize:11)))
            ),
              ]),
              PopupMenuButton(
                  icon: Icon(Icons.more_vert, color: Color(0xFFB29576)),
                  itemBuilder: (context) => [
                PopupMenuItem(
                  child: Text("Reply"),
                ),
                    PopupMenuItem(
                      child: Text("Delete"),
    ),
                      PopupMenuItem(
                        child: Text("Edit"),
                      ),
              ],
              )
            ]),
            Container(
            color: Colors.white,
            width:contWidth,
            height:86.0,
            padding: EdgeInsets.all(5.0),
            margin: EdgeInsets.all(8.0),
            child: Text("Lorem ipsum dolor sit amet",
                  style: GoogleFonts.sora(textStyle:TextStyle(
                  color: Color(0xFFB29576), letterSpacing: 0.5, fontSize:11))),
            ),

            Container(
              child:
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                    IconButton(
                      icon: Icon((liked == false)? Icons.thumb_up_alt_outlined : Icons.thumb_up_alt_sharp, size:20, color:Color(0xFFB29576)),
                      onPressed: _incrementLike,
                    ),
                        Text("${_likeCount}" ,style: TextStyle(color:Color(0xFFB29576))),
                        IconButton(
                          icon: Icon((disliked == false) ? Icons.thumb_down_alt_outlined : Icons.thumb_down_alt_sharp, size:20, color: Color(0xFFB29576)),
                          onPressed: _incrementDislike,
                        ),
                        Text("${_dislikeCount}", style: TextStyle(color:Color(0xFFB29576)))
                    ]
                  )
            )
          ]

          )),
          Container(
              color: Color(0xffAC3834),
              width:contWidth,
              height:contHeight,
              alignment: Alignment.center,
              margin: EdgeInsets.all(25.0),
              padding: EdgeInsets.all(20.0),
              child: Column(
                  children:<Widget>[
                    Row(
                        children:<Widget>[
                          Container(
                              alignment:Alignment.centerLeft,
                              child: Image.network('https://live.staticflickr.com/65535/51634618240_caba66ca65_o.png', width: 60, height:60)
                          ),
                          Column(children:<Widget>[
                            Container(
                                width:200,
                                alignment: Alignment.centerLeft,
                                child: Text("POST TITLE 1",
                                  style: GoogleFonts.bebasNeue(textStyle:TextStyle(color: Color(0xFF811112), fontSize:30)),
                                )
                            ),
                            Container(
                                width:200,
                                alignment: Alignment.centerLeft,
                                child: Text("Posted by Anonymous",
                                    style: GoogleFonts.sora(textStyle:TextStyle(
                                        color: Color(0xFFB29576), letterSpacing:1.3, fontSize:11)))
                            ),
                          ]),
                          PopupMenuButton(
                            icon: Icon(Icons.more_vert, color: Color(0xFFB29576)),
                            itemBuilder: (context) => [
                              PopupMenuItem(
                                child: Text("Reply"),
                              ),
                              PopupMenuItem(
                                child: Text("Delete"),
                              ),
                              PopupMenuItem(
                                child: Text("Edit"),
                              ),
                            ],
                          )
                        ]),
                    Container(
                      color: Colors.white,
                      width:contWidth,
                      height:86.0,
                      padding: EdgeInsets.all(5.0),
                      margin: EdgeInsets.all(8.0),
                      child: Text("Lorem ipsum dolor sit amet",
                          style: GoogleFonts.sora(textStyle:TextStyle(
                              color: Color(0xFFB29576), letterSpacing: 0.5, fontSize:11))),
                    ),

                    Container(
                        child:
                        Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              IconButton(
                                icon: Icon((liked == false)? Icons.thumb_up_alt_outlined : Icons.thumb_up_alt_sharp, size:20, color:Color(0xFFB29576)),
                                onPressed: _incrementLike,
                              ),
                              Text("${_likeCount}", style: TextStyle(color:Color(0xFFB29576))),
                              IconButton(
                                icon: Icon((disliked == false)? Icons.thumb_down_alt_outlined : Icons.thumb_down_alt_sharp, size:20, color: Color(0xFFB29576)),
                                onPressed: _incrementDislike,
                              ),
                              Text("${_dislikeCount}", style: TextStyle(color:Color(0xFFB29576)))
                            ]
                        )
                    )
                  ]

              )),
          Container(
              color: Color(0xffAC3834),
              width:contWidth,
              height:contHeight,
              alignment: Alignment.center,
              margin: EdgeInsets.all(25.0),
              padding: EdgeInsets.all(20.0),
              child: Column(
                  children:<Widget>[
                    Row(
                        children:<Widget>[
                          Container(
                              alignment:Alignment.centerLeft,
                              child: Image.network('https://live.staticflickr.com/65535/51634618240_caba66ca65_o.png', width: 60, height:60)
                          ),
                          Column(children:<Widget>[
                            Container(
                                width:200,
                                alignment: Alignment.centerLeft,
                                child: Text("POST TITLE 1",
                                  style: GoogleFonts.bebasNeue(textStyle:TextStyle(color: Color(0xFF811112), fontSize:30)),
                                )
                            ),
                            Container(
                                width:200,
                                alignment: Alignment.centerLeft,
                                child: Text("Posted by Anonymous",
                                    style: GoogleFonts.sora(textStyle:TextStyle(
                                        color: Color(0xFFB29576), letterSpacing:1.3, fontSize:11)))
                            ),
                          ]),
                          PopupMenuButton(
                            icon: Icon(Icons.more_vert, color: Color(0xFFB29576)),
                            itemBuilder: (context) => [
                              PopupMenuItem(
                                child: Text("Reply"),
                              ),
                              PopupMenuItem(
                                child: Text("Delete"),
                              ),
                              PopupMenuItem(
                                child: Text("Edit"),
                              ),
                            ],
                          )
                        ]),
                    Container(
                      color: Colors.white,
                      width:contWidth,
                      height:86.0,
                      padding: EdgeInsets.all(5.0),
                      margin: EdgeInsets.all(8.0),
                      child: Text("Lorem ipsum dolor sit amet",
                          style: GoogleFonts.sora(textStyle:TextStyle(
                              color: Color(0xFFB29576), letterSpacing: 0.5, fontSize:11))),
                    ),

                    Container(
                        child:
                        Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              IconButton(
                                icon: Icon((liked == false)? Icons.thumb_up_alt_outlined : Icons.thumb_up_alt_sharp, size:20, color:Color(0xFFB29576)),
                                onPressed: _incrementLike,
                              ),
                              Text("${_likeCount}",style: TextStyle(color:Color(0xFFB29576))),
                              IconButton(
                                icon: Icon((disliked == false)? Icons.thumb_down_alt_outlined : Icons.thumb_down_alt_sharp, size:20, color: Color(0xFFB29576)),
                                onPressed: _incrementDislike,
                              ),
                              Text("${_dislikeCount}", style: TextStyle(color:Color(0xFFB29576)))
                            ]
                        )
                    )
                  ]

              )),
    ]));
  }
}