from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'
        widgets = {'dob': forms.DateInput(format=('%m/%d/%Y'), attrs={'class':'form-control', 'placeholder':'Select a date', 'type':'date'}),}