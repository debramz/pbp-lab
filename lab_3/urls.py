from django.contrib import admin
from django.urls import path
from lab_3.views import index
from lab_3.views import add_friend

urlpatterns = [
    path('', index, name='index'),
    path('add/', add_friend, name="add"),
]