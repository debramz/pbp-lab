1. Apakah perbedaan antara JSON dan XML?
XML merupakan sebuah markup language seperti HTML, sementara JSON adalah sebuah format data.

JSON berperan sebagai suatu dictionary yang biasanya menyimpan key-value pairs berupa string sebagai key dan berbagai macam datatype sebagai value seperti list, dan lain-lain.

JSON juga cenderung mempunyai ukuran file lebih kecil dan bisa mentransfer data lebih cepat daripada XML. JSON juga mempunyai struktur yang simple dan mempunyai syntax yang minimal.

XML seringkali dikenal kompleks dan tidak modern karena struktur tag yang membuat file menjadi lebih besar dan sulit untuk dibaca. Namun, kompleksitas tersebut membuat bahasa ini mentransfer data sekaligus memproses dan memformat object dan dokumen. XML membolehkan banyak datatype seperti image dan chart, sementara JSON hanya mensupport string, object, number, dan boolean arrays.

Perbedaan terbesar antara JSON dan XML terletak pada data parsing. JSON dapat diparse secara mudah oleh sebuah JavaScript function sederhana karena sudah terintegrasi. Namun, pada XML harus diparse dengan sebuah XML parser yang lebih lamban dan sulit.

2. Apakah perbedaan antara HTML dan XML?
HTML cenderung lebih berfokus terhadap style dan cara menampilkan data pada halaman (page), sementara XML tidak melakukan hal-hal tersebut. XML ada untuk sekadar menyimpan data. Perbedaan lain antara HTML dan XML terletak pada tagsnya. Pada XML, kita dapat membuat custom tags, sementara pada HTML kita hanya punya jumlah tags terbatas yang sudah ada.