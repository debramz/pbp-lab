from django.contrib import admin
from django.urls import path
from lab_4.views import index
from lab_4.views import add_note
from lab_4.views import note_list

urlpatterns = [
    path("", index, name='index'),
    path("add-note/", add_note, name="add note"),
    path("note-list/", note_list, name="note list"),
]